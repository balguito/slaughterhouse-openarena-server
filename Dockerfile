FROM bitnami/minideb:latest

ENV OA_NAME=OpenArena \
    OA_WELCOME="The IURCO Slaughterhouse" \
	OA_PASSWORD="" \
	OA_GAME_PORT=27960 \
	OA_HTTP=http://localhost/download \
    NODE_ENV=production

ADD gamefiles /opt/openarena

ADD settings.cfg /root/.openarena/baseoa/

EXPOSE 27950/udp 27960/udp

CMD /opt/openarena/oa_ded.x86_64 +exec settings.cfg
